package ru.andreymarkelov.atlas.plugins.customuserpicker.field;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverterImpl;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserFilterManager;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;

import ru.mail.jira.plugins.up.PluginData;
import ru.mail.jira.plugins.up.ValueComparator;
import ru.mail.jira.plugins.up.common.Utils;
@Component
public class SelectedUsersCf extends UserCFType {
	
    private final PluginData data;
    private final UserUtil userUtil;
    private final String baseUrl;
    private final UserManager userManger;

    
    public SelectedUsersCf(
            final CustomFieldValuePersister customFieldValuePersister,
            final GenericConfigManager genericConfigManager,
            final ApplicationProperties applicationProperties,
            final JiraAuthenticationContext authenticationContext,
            final FieldConfigSchemeManager fieldConfigSchemeManager,
            final ProjectManager projectManager,
            final SoyTemplateRendererProvider soyTemplateRendererProvider,
            final GroupManager groupManager,
            final ProjectRoleManager projectRoleManager,
            final UserSearchService searchService,
            final JiraBaseUrls jiraBaseUrls,
            final UserHistoryManager userHistoryManager,
            final UserFilterManager userFilterManager,
            final I18nHelper i18nHelper,
            final UserBeanFactory userBeanFactory,
            final PluginData data,
            final UserUtil userUtil,
            final UserManager userManager) {
    	
    	super( customFieldValuePersister,  new UserConverterImpl(userManager,i18nHelper),  genericConfigManager,  applicationProperties,  authenticationContext,  fieldConfigSchemeManager,  projectManager,  soyTemplateRendererProvider,  groupManager,  projectRoleManager,  searchService,  jiraBaseUrls,  userHistoryManager,  userFilterManager,  i18nHelper,  userBeanFactory);
    	
        /*super(getComponent(customFieldValuePersister.getClass()), 
        		new UserConverterImpl(getComponent(userManager.getClass()), getComponent(i18nHelper.getClass())), 
        		getComponent(genericConfigManager.getClass()), getComponent(applicationProperties.getClass()), 
        		getComponent(authenticationContext.getClass()),
        		getComponent(fieldConfigSchemeManager.getClass()), getComponent(projectManager.getClass()), 
        		getComponent(soyTemplateRendererProvider.getClass()), getComponent(groupManager.getClass()), getComponent(projectRoleManager.getClass()),
        		getComponent(searchService.getClass()), getComponent(jiraBaseUrls.getClass()), 
        		getComponent(userHistoryManager.getClass()), getComponent(userFilterManager.getClass()), 
        		getComponent(i18nHelper.getClass()), getComponent(userBeanFactory.getClass()));*/
        this.data = data;
        this.userUtil = userUtil;
        this.baseUrl = "";
        this.userManger = userManager;
       
    }
    
    private static <T> T getComponent(Class<T> tClass) {
        return ComponentAccessor.getComponent(tClass);
    }
    

    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);

        Map<String, String> map = new HashMap<String, String>();
        Set<String> users = data.getStoredUsers(field.getId());
        for (String user : users)
        {
        	
            ApplicationUser userObj = this.userManger.getUserByName(user);
            if (userObj != null)
            {
                map.put(userObj.getName(), userObj.getDisplayName());
            }
        }

        TreeMap<String, String> sorted_map = new TreeMap<String, String>(
            new ValueComparator(map));
        sorted_map.putAll(map);
        params.put("map", sorted_map);
        params.put("isautocomplete", data.isAutocompleteView(field.getId()));
        params.put("baseUrl", baseUrl);

        Utils.addViewAndEditParameters(params, field.getId());

        return params;
    }
}
