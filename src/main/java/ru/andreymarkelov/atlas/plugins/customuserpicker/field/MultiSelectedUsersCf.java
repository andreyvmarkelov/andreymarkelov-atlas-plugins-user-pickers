package ru.andreymarkelov.atlas.plugins.customuserpicker.field;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.jira.ComponentManager;
import com.atlassian.jira.avatar.Avatar.Size;
import com.atlassian.jira.avatar.AvatarServiceImpl;
import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.MultiUserConverterImpl;
import com.atlassian.jira.issue.customfields.impl.MultiUserCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.web.FieldVisibilityManager;

import ru.mail.jira.plugins.up.PluginData;
import ru.mail.jira.plugins.up.ValueComparator;
import ru.mail.jira.plugins.up.common.Utils;

@Component
public class MultiSelectedUsersCf extends MultiUserCFType {
    private final PluginData data;
    private final UserUtil userUtil;
    private final String baseUrl;
    private final AvatarServiceImpl avatarService;
    private final UserManager userManager;
    
    private Map<String, String> usersAvatars;
    public MultiSelectedUsersCf(
    		final CustomFieldValuePersister customFieldValuePersister,
    		final GenericConfigManager genericConfigManager,
    		final ApplicationProperties applicationProperties,
    		final JiraAuthenticationContext authenticationContext,
    		final UserSearchService searchService,
    		final FieldVisibilityManager fieldVisibilityManager,
    		final JiraBaseUrls jiraBaseUrls,
    		final UserBeanFactory userBeanFactory,
    		final PluginData data,
    		final I18nHelper i18nHelper,
    		final UserUtil userUtil,
    		final UserManager userManager) {
    	super(customFieldValuePersister,genericConfigManager,new MultiUserConverterImpl(userManager, i18nHelper),applicationProperties,authenticationContext,
    			searchService,fieldVisibilityManager,jiraBaseUrls,userBeanFactory);
    	
    	/*
        super(getComponent(customFieldValuePersister.getClass()), getComponent(genericConfigManager.getClass()), 
        		new MultiUserConverterImpl(getComponent(userManager.getClass()), getComponent(i18nHelper.getClass())), 
        		getComponent(applicationProperties.getClass()), getComponent(authenticationContext.getClass()),
        		getComponent(searchService.getClass()), getComponent(fieldVisibilityManager.getClass()), getComponent(jiraBaseUrls.getClass()),
        		getComponent(userBeanFactory.getClass()));*/
        this.data = data;
        this.userUtil = userUtil;
        this.avatarService = ComponentManager.getComponentInstanceOfType(AvatarServiceImpl.class);
        this.baseUrl = "";
        this.userManager = userManager;
    }
    private static <T> T getComponent(Class<T> tClass) {
        return ComponentAccessor.getComponent(tClass);
    }
    
    @Override
    public Map<String, Object> getVelocityParameters(Issue issue,
        CustomField field, FieldLayoutItem fieldLayoutItem)
    {
    	
        Map<String, Object> params = super.getVelocityParameters(issue, field,
            fieldLayoutItem);

        Map<String, String> map = new HashMap<String, String>();
        Set<String> users = data.getStoredUsers(field.getId());
        for (String user : users)
        {
            ApplicationUser userObj = userManager.getUserByName(user);
            if (userObj != null)
            {
                map.put(userObj.getName(), userObj.getDisplayName());
            }
        }

        Object issueValObj = issue.getCustomFieldValue(field);
        Set<String> issueVal = Utils.convertList(issueValObj);
        params.put("selectVal", Utils.convertSetToString(issueVal));

        TreeMap<String, String> sorted_map = new TreeMap<String, String>(
            new ValueComparator(map));
        sorted_map.putAll(map);
        params.put("map", sorted_map);
        params.put("issueVal", issueVal);
        params.put("isautocomplete", data.isAutocompleteView(field.getId()));
        params.put("baseUrl", baseUrl);

        usersAvatars = new HashMap<String, String>(users.size());
        for (String userName : users)
        {
        	
            ApplicationUser user = userManager.getUserByName(userName);
            if (user != null)
            {
                usersAvatars.put(user.getName(), getUserAvatarUrl(user));
            }
        }
        params.put("usersAvatars", usersAvatars);

        Utils.addViewAndEditParameters(params, field.getId());

        return params;
    }

    private String getUserAvatarUrl(ApplicationUser user)
    {
        URI uri = avatarService.getAvatarAbsoluteURL(user, user.getName(), Size.SMALL);

        return uri.toString();
    }
}
