package ru.andreymarkelov.atlas.plugins.customuserpicker.upgradetask;

import java.util.Collection;

import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;

public class UpgradeFieldSettingsTask implements PluginUpgradeTask {

    @Override
    public Collection<Message> doUpgrade() throws Exception {
        return null;
    }

    @Override
    public int getBuildNumber() {
        return 1;
    }

    @Override
    public String getPluginKey() {
        return null;
    }

    @Override
    public String getShortDescription() {
        return null;
    }
}
