package ru.andreymarkelov.atlas.plugins.customuserpicker.action;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.Permissions;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.sal.api.ApplicationProperties;

import ru.mail.jira.plugins.up.PluginData;
import ru.mail.jira.plugins.up.SelectedFieldData;
import ru.mail.jira.plugins.up.common.Consts;
import ru.mail.jira.plugins.up.common.Utils;
import ru.mail.jira.plugins.up.structures.FieldData;
import ru.mail.jira.plugins.up.structures.ProjRole;

public class UserPickerAdminAction extends JiraWebActionSupport {
    private static final long serialVersionUID = 8016730182922672388L;

    private final ApplicationProperties applicationProperties;
    private final CustomFieldManager cfMgr;
    private final PluginData data;
    private Map<String, FieldData> multiFields;
    private final ProjectManager prMgr;
    private final ProjectRoleManager projectRoleManager;

    private Map<String, SelectedFieldData> multiSelectedFields;
    private Map<String, FieldData> singleFields;
    private Map<String, SelectedFieldData> singleSelectedFields;

    public UserPickerAdminAction(ApplicationProperties applicationProperties,
        CustomFieldManager cfMgr, ProjectManager prMgr,
        ProjectRoleManager projectRoleManager, PluginData data)
    {
        this.applicationProperties = applicationProperties;
        this.cfMgr = cfMgr;
        this.prMgr = prMgr;
        this.projectRoleManager = projectRoleManager;
        this.data = data;
        this.singleFields = new LinkedHashMap<String, FieldData>();
        this.multiFields = new LinkedHashMap<String, FieldData>();
        this.singleSelectedFields = new LinkedHashMap<String, SelectedFieldData>();
        this.multiSelectedFields = new LinkedHashMap<String, SelectedFieldData>();
    }

    @Override
    public String doDefault() throws Exception
    {
        List<CustomField> cgList = cfMgr.getCustomFieldObjects();
        for (CustomField cf : cgList)
        {
            if (cf.getCustomFieldType().getKey()
                .equals(Consts.CF_KEY_SINGLE_USER_GR_ROLE_SELECT)
                || cf.getCustomFieldType().getKey()
                    .equals(Consts.CF_KEY_MULTI_USER_GR_ROLE_SELECT))
            {
                FieldData fdata = new FieldData(cf.getId(), cf.getName());
                if (cf.isAllProjects())
                {
                    fdata.setAllProjects(true);
                }
                else
                {
                    fdata.setAllProjects(false);
                    List<String> fieldProjs = new ArrayList<String>();
                    List<Project> projs = cf.getAssociatedProjectObjects();
                    for (Project proj : projs)
                    {
                        fieldProjs.add(proj.getName());
                    }

                    fdata.setProjects(fieldProjs);
                }

                List<String> groups = new ArrayList<String>();
                List<ProjRole> projRoles = new ArrayList<ProjRole>();
                try
                {
                    Utils.fillDataLists(data.getRoleGroupFieldData(cf.getId()),
                        groups, projRoles);
                }
                catch (JSONException e)
                {
                    log.error(
                        "AdRoleGroupUserCfService::fillLists - Incorrect field data",
                        e);
                    // --> impossible
                }
                fdata.setAutocomplete(data.isAutocompleteView(cf.getId()));
                fdata.getGroups().addAll(groups);
                fdata.getRoles().addAll(projRoles);

                List<String> highlightedGroups = new ArrayList<String>();
                List<ProjRole> highlightedProjRoles = new ArrayList<ProjRole>();
                try
                {
                    Utils.fillDataLists(
                        data.getHighlightedRoleGroupFieldData(cf.getId()),
                        highlightedGroups, highlightedProjRoles);
                }
                catch (JSONException e)
                {
                    log.error(
                        "AdRoleGroupUserCfService::fillLists - Incorrect field data",
                        e);
                    // --> impossible
                }
                fdata.getHighlightedGroups().addAll(highlightedGroups);
                fdata.getHighlightedRoles().addAll(highlightedProjRoles);

                if (cf.getCustomFieldType().getKey()
                    .equals(Consts.CF_KEY_SINGLE_USER_GR_ROLE_SELECT))
                {
                    singleFields.put(fdata.getFieldId(), fdata);
                }
                else
                {
                    multiFields.put(fdata.getFieldId(), fdata);
                }
            }
            else if (cf.getCustomFieldType().getKey()
                .equals(Consts.CF_KEY_SINGLE_USER_SELECT)
                || cf.getCustomFieldType().getKey()
                    .equals(Consts.CF_KEY_MULTI_USER_SELECT))
            {
                SelectedFieldData fdata = new SelectedFieldData(cf.getId(),
                    cf.getName());
                fdata.addUsers(data.getStoredUsers(cf.getId()));
                if (cf.isAllProjects())
                {
                    fdata.setAllProjects(true);
                }
                else
                {
                    fdata.setAllProjects(false);
                    List<String> fieldProjs = new ArrayList<String>();
                    List<Project> projs = cf.getAssociatedProjectObjects();
                    for (Project proj : projs)
                    {
                        fieldProjs.add(proj.getName());
                    }
                    fdata.setProjects(fieldProjs);
                }
                fdata.setAutocomplete(data.isAutocompleteView(cf.getId()));

                if (cf.getCustomFieldType().getKey()
                    .equals(Consts.CF_KEY_SINGLE_USER_SELECT))
                {
                    singleSelectedFields.put(fdata.getFieldId(), fdata);
                }
                else
                {
                    multiSelectedFields.put(fdata.getFieldId(), fdata);
                }
            }
        }

        return SUCCESS;
    }

    /**
     * Get context path.
     */
    public String getBaseUrl()
    {
    	return ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL);
    }

    public Map<String, FieldData> getMultiFields()
    {
        return multiFields;
    }

    public Map<String, SelectedFieldData> getMultiSelectedFields()
    {
        return multiSelectedFields;
    }

    /**
     * Get all projects.
     */
    public Map<String, String> getProjectMap()
    {
        List<Project> projects = prMgr.getProjectObjects();
        Map<String, String> projs = new TreeMap<String, String>();
        if (projects != null)
        {
            for (Project project : projects)
            {
                projs.put(project.getId().toString(), project.getName());
            }
        }

        return projs;
    }

    /**
     * Get all roles.
     */
    public Map<String, String> getRoleMap()
    {
        Map<String, String> roleProjs = new TreeMap<String, String>();
        Collection<ProjectRole> roles = projectRoleManager.getProjectRoles();
        if (roles != null)
        {
            for (ProjectRole role : roles)
            {
                roleProjs.put(role.getId().toString(), role.getName());
            }
        }

        return roleProjs;
    }

    public Map<String, FieldData> getSingleFields()
    {
        return singleFields;
    }

    public Map<String, SelectedFieldData> getSingleSelectedFields()
    {
        return singleSelectedFields;
    }

    /**
     * Check administer permissions.
     */
    public boolean hasAdminPermission()
    {
        ApplicationUser user = getLoggedInUser();
        if (user == null)
        {
            return false;
        }
        
        if (this.getGlobalPermissionManager().hasPermission(GlobalPermissionKey.ADMINISTER,getLoggedInUser()))
        {
            return true;
        }

        return false;
    }
}
