package ru.andreymarkelov.atlas.plugins.customuserpicker.field;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

import org.osgi.service.component.annotations.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.converters.UserConverter;
import com.atlassian.jira.issue.customfields.converters.UserConverterImpl;
import com.atlassian.jira.issue.customfields.impl.UserCFType;
import com.atlassian.jira.issue.customfields.manager.GenericConfigManager;
import com.atlassian.jira.issue.customfields.persistence.CustomFieldValuePersister;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;
import com.atlassian.jira.issue.fields.rest.json.UserBeanFactory;
import com.atlassian.jira.issue.fields.rest.json.beans.JiraBaseUrls;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.template.soy.SoyTemplateRendererProvider;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.UserFilterManager;
import com.atlassian.jira.user.UserHistoryManager;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.jira.util.json.JSONException;

import ru.mail.jira.plugins.up.PluginData;
import ru.mail.jira.plugins.up.common.Utils;
import ru.mail.jira.plugins.up.structures.ProjRole;

@Component
public class RoleGroupUserField extends UserCFType {
    private final PluginData data;
    private final GroupManager grMgr;
    private final ProjectRoleManager projectRoleManager;
    private final String baseUrl;
    

    
    public RoleGroupUserField(
            final CustomFieldValuePersister customFieldValuePersister,
            final GenericConfigManager genericConfigManager,
            final ApplicationProperties applicationProperties,
            final JiraAuthenticationContext authenticationContext,
            final FieldConfigSchemeManager fieldConfigSchemeManager,
            final ProjectManager projectManager,
            final SoyTemplateRendererProvider soyTemplateRendererProvider,
            final GroupManager groupManager,
            final ProjectRoleManager projectRoleManager,
            final UserSearchService searchService,
            final JiraBaseUrls jiraBaseUrls,
            final UserHistoryManager userHistoryManager,
            final UserFilterManager userFilterManager,
            final I18nHelper i18nHelper,
            final UserBeanFactory userBeanFactory,
            final PluginData data,
            final GroupManager grMgr,
            final UserManager userManager) {
      
    	
    	super(customFieldValuePersister, new UserConverterImpl(userManager, i18nHelper), genericConfigManager, applicationProperties, authenticationContext, fieldConfigSchemeManager
    			, projectManager, soyTemplateRendererProvider, groupManager, projectRoleManager, searchService, jiraBaseUrls, 
    			userHistoryManager,  userFilterManager,  i18nHelper, userBeanFactory);
    	
    	/*super(getComponent(customFieldValuePersister.getClass()), 
        		new UserConverterImpl(getComponent(userManager.getClass()), getComponent(i18nHelper.getClass())), 
        		getComponent(genericConfigManager.getClass()), getComponent(applicationProperties.getClass()), getComponent(authenticationContext.getClass()),
        		getComponent(fieldConfigSchemeManager.getClass()), getComponent(projectManager.getClass()), 
        		getComponent(soyTemplateRendererProvider.getClass()), getComponent(groupManager.getClass()), getComponent(projectRoleManager.getClass()),
        		getComponent(searchService.getClass()), getComponent(jiraBaseUrls.getClass()), 
        		getComponent(userHistoryManager.getClass()), getComponent(userFilterManager.getClass()), 
        		getComponent(i18nHelper.getClass()), getComponent(userBeanFactory.getClass()));*/
    	
        this.data = data;
        this.grMgr = grMgr;
        this.projectRoleManager = projectRoleManager;
        this.baseUrl = "";
    }

    private static <T> T getComponent(Class<T> tClass) {
        return ComponentAccessor.getComponent(tClass);
    }
    
    @Override
    public Map<String, Object> getVelocityParameters(Issue issue, CustomField field, FieldLayoutItem fieldLayoutItem) {
        Map<String, Object> params = super.getVelocityParameters(issue, field, fieldLayoutItem);

        List<String> groups = new ArrayList<String>();
        List<ProjRole> projRoles = new ArrayList<ProjRole>();
        try {
            Utils.fillDataLists(data.getRoleGroupFieldData(field.getId()), groups, projRoles);
        } catch (JSONException e) {
            log.error("RoleGroupUserField::getVelocityParameters - Incorrect field data", e);
        }

        List<String> highlightedGroups = new ArrayList<String>();
        List<ProjRole> highlightedProjRoles = new ArrayList<ProjRole>();
        try {
            Utils.fillDataLists(data.getHighlightedRoleGroupFieldData(field.getId()), highlightedGroups, highlightedProjRoles);
        } catch (JSONException e) {
            log.error("RoleGroupUserField::getVelocityParameters - Incorrect field data", e);
        }

        /* Build possible values list */
        
        Project project = null;
        if(issue != null){
        	project = issue.getProjectObject();
        }

        SortedSet<ApplicationUser> possibleUsers = Utils.buildUsersList(grMgr,
            projectRoleManager, project, groups, projRoles);
        Set<ApplicationUser> allUsers = new HashSet<ApplicationUser>(possibleUsers);
        SortedSet<ApplicationUser> highlightedUsers = Utils.buildUsersList(grMgr,
            projectRoleManager, project, highlightedGroups,
            highlightedProjRoles);
        highlightedUsers.retainAll(possibleUsers);
        possibleUsers.removeAll(highlightedUsers);

        Map<String, String> highlightedUsersSorted = new LinkedHashMap<String, String>();
        Map<String, String> otherUsersSorted = new LinkedHashMap<String, String>();
        for (ApplicationUser user : highlightedUsers)
        {
            highlightedUsersSorted.put(user.getName(), user.getDisplayName());
        }
        for (ApplicationUser user : possibleUsers)
        {
            otherUsersSorted.put(user.getName(), user.getDisplayName());
        }

        params.put("allUsers", allUsers);
        params.put("isautocomplete", data.isAutocompleteView(field.getId()));
        params.put("baseUrl", baseUrl);
        params.put("highlightedUsersSorted", highlightedUsersSorted);
        params.put("otherUsersSorted", otherUsersSorted);

        Utils.addViewAndEditParameters(params, field.getId());

        return params;
    }
}
