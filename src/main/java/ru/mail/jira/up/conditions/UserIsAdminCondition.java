package ru.mail.jira.up.conditions;

import org.springframework.stereotype.Component;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.plugin.webfragment.conditions.AbstractWebCondition;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.user.ApplicationUser;
@Component
public class UserIsAdminCondition extends AbstractWebCondition {
    @Override
    public boolean shouldDisplay(ApplicationUser u, JiraHelper jh) {
        return u != null && ComponentAccessor.getUserUtil().getJiraAdministrators().contains(u);
    }

	
}