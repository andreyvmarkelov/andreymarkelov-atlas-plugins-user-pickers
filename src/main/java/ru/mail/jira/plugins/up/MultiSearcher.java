package ru.mail.jira.plugins.up;

import org.springframework.stereotype.Component;

import com.atlassian.jira.bc.user.search.UserSearchService;
import com.atlassian.jira.issue.customfields.converters.MultiUserConverter;
import com.atlassian.jira.issue.customfields.searchers.UserPickerSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.jql.resolver.UserResolver;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.util.UserManager;
import com.atlassian.jira.util.EmailFormatter;
import com.atlassian.jira.web.FieldVisibilityManager;

@Component
public class MultiSearcher extends UserPickerSearcher {
	
    public MultiSearcher(UserResolver userResolver, JqlOperandResolver operandResolver,
            JiraAuthenticationContext context, MultiUserConverter userConverter, UserSearchService userSearchService,
            CustomFieldInputHelper customFieldInputHelper, UserManager userManager,
            FieldVisibilityManager fieldVisibilityManager, EmailFormatter emailFormatter) {
        super(userResolver, operandResolver, context, userConverter, userSearchService, customFieldInputHelper, userManager,
                fieldVisibilityManager, emailFormatter);
    }
}
