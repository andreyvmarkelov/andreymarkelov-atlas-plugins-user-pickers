	package ru.mail.jira.plugins.up;

public class JIRACustomField {
	private Long id;
	private String cfId;
	public JIRACustomField(long id, String cfId) {
		this.id = id;
		this.cfId = cfId;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCfId() {
		return cfId;
	}
	public void setCfId(String cfId) {
		this.cfId = cfId;
	}
		
}
